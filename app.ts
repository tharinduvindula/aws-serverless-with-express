import 'source-map-support/register';

import * as express from 'express';
import awsServerlessExpressMiddleware from 'aws-serverless-express/middleware';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';

export const app = express();
const router = express.Router();


router.use(cookieParser());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(cors({ origin: true }));
router.use(awsServerlessExpressMiddleware.eventContext())


router.get('/', (_req: express.Request, res: express.Response) => {
  res.json({ hello: 'world' });
});

router.get('/users', (_req: express.Request, res: express.Response) => {
  res.json(users)
})

router.get('/users/:userId', (req: express.Request, res: express.Response) => {
  const user = getUser(req)

  if (!user) return res.status(404).json({})

  return res.json(user)
})

router.post('/users', (req: express.Request, res: express.Response) => {
  const user = {
    id: ++userIdCounter,
    name: req.body.name
  }
  users.push(user)
  res.status(201).json(user)
})

router.put('/users/:userId', (req: express.Request, res: express.Response) => {
  const user = getUser(req)

  if (!user) return res.status(404).json({})

  user.name = req.body.name
  res.json(user)
})

router.delete('/users/:userId', (req: express.Request, res: express.Response) => {
  const userIndex = getUserIndex(req)

  if (userIndex === -1) return res.status(404).json({})

  users.splice(userIndex, 1)
  res.json(users)
})

const getUser = (userId) => users.find(u => u.id === parseInt(userId))
const getUserIndex = (userId) => users.findIndex(u => u.id === parseInt(userId))

// Ephemeral in-memory data store
const users = [{
  id: 1,
  name: 'Joe'
}, {
  id: 2,
  name: 'Jane'
}]
let userIdCounter = users.length;

app.use('/', router);
